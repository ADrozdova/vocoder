# Vocoder



## Installation guide
```
pip install -r ./requirements.txt -q
```
Download LJSpeech dataset:

```
./get_ljspeech.sh
```


Download checkpoint (model in `checkpoint.pth`, config in `config.json`):

```
python download_checkpoint.py
```

Run test

```
python test.py -c test_config.json -r checkpoint.pth
```