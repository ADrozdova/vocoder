from google_drive_downloader import GoogleDriveDownloader as gdd
import os

GOOGLE_DRIVE_CHECKPOINT_ID = "1_EmsAaWleUTPbXTNLUKal8lJ4A6VtrYv"
GOOGLE_DRIVE_CONFIG_ID = "176t26XRjwnzK0xx3ZFrdYaeWjdq5J2qj"

DIR = "."


if not os.path.exists(DIR):
    os.mkdir(DIR)

gdd.download_file_from_google_drive(file_id=GOOGLE_DRIVE_CHECKPOINT_ID,
                                    dest_path=os.path.join(DIR, "checkpoint.pth"),
                                    unzip=False)
gdd.download_file_from_google_drive(file_id=GOOGLE_DRIVE_CONFIG_ID,
                                    dest_path=os.path.join(DIR, "checkpoint_config.json"),
                                    unzip=False)
