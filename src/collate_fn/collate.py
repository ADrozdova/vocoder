from typing import Tuple, List, Dict

from torch.nn.utils.rnn import pad_sequence


class LJSpeechCollator:

    def __call__(self, instances: List[Tuple]) -> Dict:
        index, waveform, transcript = list(
            zip(*instances)
        )

        waveform = pad_sequence([
            waveform_ for waveform_ in waveform
        ]).transpose(0, 1)

        return {
            "waveform": waveform,
            "transcript": transcript,
        }

