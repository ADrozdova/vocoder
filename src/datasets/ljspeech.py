import random

import torchaudio
from torch.nn.functional import pad

LJ_ABBR = {
    "Mr.":	"Mister",
    "Mrs.":	"Misess",
    "Dr.":	"Doctor",
    "No.":	"Number",
    "St.":	"Saint",
    "Co.":	"Company",
    "Jr.":	"Junior",
    "Maj.":	"Major",
    "Gen.":	"General",
    "Drs.":	"Doctors",
    "Rev.":	"Reverend",
    "Lt.":	"Lieutenant",
    "Hon.":	"Honorable",
    "Sgt.":	"Sergeant",
    "Capt.":	"Captain",
    "Esq.":	"Esquire",
    "Ltd.":	"Limited",
    "Col.":	"Colonel",
    "Ft.":	"Fort",
}


def fix_text(text):
    to_replace = list('"üêàéâè”“’[]')
    replacements = [""] + list("ueaeae") + ["", "", "'", "", ""]
    for c, rep in zip(to_replace, replacements):
        text = text.replace(c, rep)
    for abbr, word in LJ_ABBR.items():
        text = text.replace(abbr, word)
    return text


class LJSpeechDataset(torchaudio.datasets.LJSPEECH):

    def __init__(self, root, part, segment_size=None):
        super().__init__(root=root)
        self.part = part
        len_all = super().__len__()
        self.train_len = int(0.8 * len_all)
        self.val_len = len_all - self.train_len
        self.segment_size = segment_size

    def __len__(self):
        if self.part == "train":
            return self.train_len
        if self.part == "val":
            return self.val_len

    def __getitem__(self, index: int):
        if self.part == "val":
            index += self.train_len
        waveform, _, _, transcript = super().__getitem__(index)

        if self.segment_size is not None:
            if self.segment_size <= waveform.shape[1]:
                max_audio_start = waveform.shape[1] - self.segment_size
                audio_start = random.randint(0, max_audio_start)
                waveform = waveform[:, audio_start:audio_start + self.segment_size]
            else:
                waveform = pad(waveform, (0, self.segment_size - waveform.shape[1]), 'constant')

        transcript = fix_text(transcript)

        return index, waveform, transcript

