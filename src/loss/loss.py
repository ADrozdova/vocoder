from torch import nn
import torch.nn.functional as F
import torch


class HiFiLoss(nn.Module):
    def __init__(self):
        super(HiFiLoss, self).__init__()
        self.mel_loss = MelLoss()
        self.feat_loss = FeatureLoss()
        self.gen_loss = GeneratorLoss()
        self.disc_loss = DiscriminatorLoss()

    def forward(self, a, b=None, type="MelLoss"):
        if type == "MelLoss":
            return self.mel_loss(a, b)
        elif type == "FeatureLoss":
            return self.feat_loss(a, b)
        elif type == "GeneratorLoss":
            return self.gen_loss(a)
        elif type == "DiscriminatorLoss":
            return self.disc_loss(a, b)
        else:
            raise Exception("Loss of type " + type + " is not in HiFiLoss")


class MelLoss(nn.Module):
    def __init__(self):
        super(MelLoss, self).__init__()

    def forward(self, melspec, melspec_gen):
        return F.l1_loss(melspec_gen, melspec)


class FeatureLoss(nn.Module):
    def __init__(self):
        super(FeatureLoss, self).__init__()

    def forward(self, fmap_real, fmap_gen):
        loss = 0
        for real, gen in zip(fmap_real, fmap_gen):
            for real_i, gen_i in zip(real, gen):
                loss += F.l1_loss(real_i, gen_i)
        return loss * 2


class GeneratorLoss(nn.Module):
    def __init__(self):
        super(GeneratorLoss, self).__init__()

    def forward(self, disc_outputs):
        loss = 0
        gen_losses = []
        for dg in disc_outputs:
            l = F.mse_loss(dg, torch.ones_like(dg))
            gen_losses.append(l)
            loss += l
        return loss, gen_losses


class DiscriminatorLoss(nn.Module):
    def __init__(self):
        super(DiscriminatorLoss, self).__init__()

    def forward(self, disc_real_out, disc_generated_out):
        loss = 0
        real_losses = []
        gen_losses = []
        for dr, dg in zip(disc_real_out, disc_generated_out):
            r_loss = F.mse_loss(dr, torch.ones_like(dr))
            g_loss = F.mse_loss(dg, torch.zeros_like(dg))

            loss += (r_loss + g_loss)
            real_losses.append(r_loss.item())
            gen_losses.append(g_loss.item())
        return loss, real_losses, gen_losses