import torch
import torch.nn.functional as F
from torch import nn
from torch.nn.utils import remove_weight_norm
from torch.nn.utils import weight_norm, spectral_norm

from src.model.utils import get_padding


class ResnetBlock(nn.Module):
    def __init__(self, hidden_channels, kernel_size, dilations, leaky_relu=0.2):
        super(ResnetBlock, self).__init__()
        layers = []

        for i in range(len(dilations)):
            conv = nn.Conv1d(
                hidden_channels, hidden_channels, kernel_size[i], dilation=dilations[i],
                padding=get_padding(kernel_size[i], dilations[i])
            )
            layers.append(weight_norm(conv))
            layers.append(nn.LeakyReLU(leaky_relu))

        self.layers = nn.Sequential(*layers)

    def remove_weight_norm(self):
        for layer in self.layers:
            remove_weight_norm(layer)

    def forward(self, x):
        output = x.clone()
        for layer in self.layers:
            output = layer(output)
        return output + x


class ResnetBlockBlock(nn.Module):
    def __init__(self, hidden_channels, kernel_size, dilations, leaky_relu=0.2):
        super(ResnetBlockBlock, self).__init__()
        layers = []
        for i in range(len(dilations)):
            layers.append(ResnetBlock(hidden_channels, kernel_size, dilations[i], leaky_relu))
        self.layers = nn.Sequential(*layers)

    def remove_weight_norm(self):
        for layer in self.layers:
            layer.remove_weight_norm()

    def forward(self, x):
        return self.layers(x)


class MRFBlock(nn.Module):
    def __init__(self, n_blocks, hidden_channels, kernel_size, dilations, leaky_relu=0.2):
        super(MRFBlock, self).__init__()
        self.n_blocks = n_blocks
        layers = []
        for i in range(n_blocks):
            layers.append(ResnetBlockBlock(hidden_channels, kernel_size, dilations, leaky_relu))
        self.layers = nn.Sequential(*layers)

    def remove_weight_norm(self):
        for layer in self.layers:
            layer.remove_weight_norm()

    def forward(self, x):
        output = torch.zeros_like(x)
        for layer in self.layers:
            output += layer(x)
        return output / self.n_blocks


class MPBlock(nn.Module):
    def __init__(self, period, channels, kernel_size=5, stride=3, use_spectral_norm=False, leaky_relu=0.2):
        super(MPBlock, self).__init__()
        self.period = period
        self.leaky_relu = leaky_relu
        norm_f = spectral_norm if use_spectral_norm else weight_norm

        self.convs = nn.ModuleList()
        in_ch = 1
        for i in range(len(channels)):
            self.convs.append(norm_f(nn.Conv2d(in_ch, channels[i], kernel_size=(kernel_size, 1), stride=(stride, 1),
                                               padding=(get_padding(kernel_size, 1), 0))))
            in_ch = channels[i]

        self.conv_post = norm_f(nn.Conv2d(channels[-1], 1, kernel_size=(3, 1), stride=1, padding=(1, 0)))

    def forward(self, x):
        fmap = []

        b, c, t = x.shape
        if t % self.period != 0:  # pad first
            padding = self.period - (t % self.period)
            x = F.pad(x, (0, padding), "reflect")
            t = t + padding
        x = x.view(b, c, t // self.period, self.period)

        for layer in self.convs:
            x = F.leaky_relu(layer(x), self.leaky_relu)
            fmap.append(x.clone())
        x = self.conv_post(x)
        fmap.append(x)
        return x.squeeze(1), fmap


class MSBlock(nn.Module):
    def __init__(self, channels, kernel_sizes, strides, groups, use_spectral_norm=False, leaky_relu=0.2):
        super(MSBlock, self).__init__()
        norm_f = spectral_norm if use_spectral_norm else weight_norm
        self.leaky_relu = leaky_relu

        self.convs = nn.ModuleList()
        in_ch = 1
        for i in range(len(channels)):
            self.convs.append(norm_f(nn.Conv1d(in_ch, channels[i], kernel_size=kernel_sizes[i], stride=strides[i],
                                               groups=groups[i], padding=get_padding(kernel_sizes[i]))))
            in_ch = channels[i]

        self.conv_post = norm_f(nn.Conv1d(channels[-1], 1, 3, padding=1))

    def forward(self, x):
        fmap = []
        for layer in self.convs:
            x = F.leaky_relu(layer(x), self.leaky_relu)
            fmap.append(x)
        x = self.conv_post(x)
        fmap.append(x)
        return x.squeeze(1), fmap
