import torch
import torch.nn.functional as F
from torch import nn
from torch.nn.utils import remove_weight_norm
from torch.nn.utils import weight_norm

from src.model.blocks import MRFBlock
from src.model.utils import get_padding
from src.base import BaseModel


class Generator(BaseModel):
    def __init__(self, n_mels, preconv_kernel, postconv_kernel, conv_kernel_sizes, n_mrf,
                 hidden, mrf_kernels, dilations, leaky_relu=0.2, *args, **kwargs):
        super(Generator, self).__init__()

        self.leaky_relu = leaky_relu

        preconv = nn.Conv1d(n_mels, hidden, kernel_size=preconv_kernel,
                            padding=get_padding(preconv_kernel))

        self.pre_conv = weight_norm(preconv)

        ups = []
        channels = hidden
        for i in range(len(conv_kernel_sizes)):
            stride = conv_kernel_sizes[i] // 2
            padding = (conv_kernel_sizes[i] - stride) // 2
            channels = channels // 2

            ups.append(
                nn.Sequential(
                    nn.LeakyReLU(leaky_relu),
                    nn.utils.weight_norm(
                        nn.ConvTranspose1d(channels * 2, channels,
                                           conv_kernel_sizes[i], stride=stride, padding=padding)
                    ),
                    MRFBlock(n_mrf, channels, mrf_kernels, dilations, leaky_relu)
                )
            )

        self.ups = nn.Sequential(*ups)

        self.post_conv = nn.utils.weight_norm(nn.Conv1d(channels, 1, postconv_kernel,
                                                        padding=get_padding(postconv_kernel)))
        self.leaky_relu = leaky_relu

    def forward(self, x, *args, **kwargs):
        out = self.ups(self.pre_conv(x))
        out = self.post_conv(F.leaky_relu(out, negative_slope=self.leaky_relu))
        return torch.tanh(out)

    def remove_weight_norm(self):
        remove_weight_norm(self.pre_conv)
        remove_weight_norm(self.post_conv)
        for i in range(len(self.layers)):
            remove_weight_norm(self.layers[i][1])
            self.layers[i][-1].remove_weight_norm()
