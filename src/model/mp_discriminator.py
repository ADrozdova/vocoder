from abc import ABC

from torch import nn

from src.base import BaseModel
from src.model.blocks import MPBlock


class MPDiscriminator(BaseModel):
    def __init__(self, periods, channels, *args, **kwargs):
        super(MPDiscriminator, self).__init__()

        self.blocks = nn.ModuleList()
        for p in periods:
            self.blocks.append(MPBlock(p, channels))

    def forward(self, waveform_r, waveform_g, *args, **kwargs):
        out_real, out_gen, fmaps_real, fmaps_gen = [], [], [], []

        for d in self.blocks:
            out_r, fmap_r = d(waveform_r)
            out_f, fmap_g = d(waveform_g)

            out_real.append(out_r)
            fmaps_real.append(fmap_r)

            out_gen.append(out_f)
            fmaps_gen.append(fmap_g)

        return out_real, out_gen, fmaps_real, fmaps_gen
