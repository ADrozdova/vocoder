from torch import nn

from src.base import BaseModel
from src.model.blocks import MSBlock
from src.model.utils import get_padding


class MSDiscriminator(BaseModel):
    def __init__(self, channels, kernel_sizes, strides, groups, use_spectral_norms, pool_kernel, pool_stride,
                 leaky_relu=0.2):
        super(MSDiscriminator, self).__init__()

        self.blocks = nn.ModuleList()
        for usn in use_spectral_norms:
            self.blocks.append(MSBlock(channels, kernel_sizes, strides, groups, usn, leaky_relu))

        self.meanpools = nn.ModuleList()
        for _ in range(len(self.blocks) - 1):
            self.meanpools.append(nn.AvgPool1d(pool_kernel, pool_stride, padding=get_padding(pool_kernel)))

    def forward(self, waveform_r, waveform_g, *args, **kwargs):
        out_real, out_gen, fmaps_real, fmaps_gen = [], [], [], []
        for i, d in enumerate(self.blocks):
            if i != 0:
                waveform_r = self.meanpools[i - 1](waveform_r)
                waveform_g = self.meanpools[i - 1](waveform_g)
            out_r, fmap_r = d(waveform_r)
            out_g, fmap_g = d(waveform_g)

            out_real.append(out_r)
            fmaps_real.append(fmap_r)

            out_gen.append(out_g)
            fmaps_gen.append(fmap_g)

        return out_real, out_gen, fmaps_real, fmaps_gen
