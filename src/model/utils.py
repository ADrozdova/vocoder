from torch.nn.utils import weight_norm


def init_weights(m, mu=0., sigma=0.01):
    if m.__class__.__name__.find("Conv") != -1:
        m.weight.data.normal_(mu, sigma)


def get_padding(kernel_size, dilation=1):
    return (kernel_size - 1) * dilation // 2


def apply_weight_norm(m):
    if m.__class__.__name__.find("Conv") != -1:
        weight_norm(m)
