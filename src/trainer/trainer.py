import random

import PIL
import torch
from torch.nn.utils import clip_grad_norm_
from torchvision.transforms import ToTensor
from tqdm import tqdm

from src.base import BaseTrainer
from src.logger.utils import plot_spectrogram_to_buf
from src.utils import inf_loop, MetricTracker


class Trainer(BaseTrainer):
    """
    Trainer class
    """

    def __init__(
            self,
            gen,
            disc_ms,
            disc_mp,
            criterion,
            optimizer_g,
            optimizer_d,
            config,
            device,
            data_loader,
            featurizer,
            valid_data_loader=None,
            lr_scheduler_g=None,
            lr_scheduler_d=None,
            len_epoch=None,
            skip_oom=True,
    ):
        super().__init__(gen, disc_ms, disc_mp, criterion, optimizer_g, optimizer_d, config, device)
        self.skip_oom = skip_oom
        self.config = config
        self.data_loader = data_loader
        if len_epoch is None:
            # epoch-based training
            self.len_epoch = len(self.data_loader)
        else:
            # iteration-based training
            self.data_loader = inf_loop(data_loader)
            self.len_epoch = len_epoch
        self.valid_data_loader = valid_data_loader
        self.do_validation = self.valid_data_loader is not None
        self.gen_scheduler = lr_scheduler_g
        self.disc_scheduler = lr_scheduler_d
        self.log_step = 10
        self.featurizer = featurizer

        self.train_metrics = MetricTracker(
            "mel_loss", "feat_loss", "gen_loss", "disc_loss", "grad norm", writer=self.writer
        )
        self.valid_metrics = MetricTracker(
            "mel_loss", "feat_loss", "gen_loss", "disc_loss", writer=self.writer
        )

    @staticmethod
    def move_batch_to_device(batch, device: torch.device):
        """
        Move all necessary tensors to the HPU
        """
        batch["waveform"] = batch["waveform"].to(device)
        if "melspec" in batch:
            batch["melspec"] = batch["melspec"].to(device)
        if "melspec_gen" in batch:
            batch["melspec_gen"] = batch["melspec_gen"].to(device)
        if "waveform_gen" in batch:
            batch["waveform_gen"] = batch["waveform_gen"].to(device)
        return batch

    def _clip_grad_norm(self):
        if self.config["trainer"].get("grad_norm_clip", None) is not None:
            clip_grad_norm_(
                self.gen.parameters(), self.config["trainer"]["grad_norm_clip"]
            )
            clip_grad_norm_(
                self.disc_mp.parameters(), self.config["trainer"]["grad_norm_clip"]
            )
            clip_grad_norm_(
                self.disc_ms.parameters(), self.config["trainer"]["grad_norm_clip"]
            )

    def _train_epoch(self, epoch):
        """
        Training logic for an epoch

        :param epoch: Integer, current training epoch.
        :return: A log that contains average loss and metric in this epoch.
        """
        self.gen.train()
        self.disc_mp.train()
        self.disc_ms.train()

        self.train_metrics.reset()
        self.writer.add_scalar("epoch", epoch)
        for batch_idx, batch in enumerate(
                tqdm(self.data_loader, desc="train", total=self.len_epoch)
        ):
            try:
                batch = self.process_batch(
                    batch,
                    is_train=True,
                    metrics=self.train_metrics,
                    gen_step=(batch_idx % 2 == 0)
                )
            except RuntimeError as e:
                if "out of memory" in str(e) and self.skip_oom:
                    self.logger.warning("OOM on batch. Skipping batch.")
                    for p in self.gen.parameters():
                        if p.grad is not None:
                            del p.grad  # free some memory
                    torch.cuda.empty_cache()
                    continue
                elif "Error loading audio file" in str(e):
                    self.logger.warning("failed to open file on batch. Skipping batch.")
                    for p in self.gen.parameters():
                        if p.grad is not None:
                            del p.grad  # free some memory
                    for p in self.disc_ms.parameters():
                        if p.grad is not None:
                            del p.grad  # free some memory
                    for p in self.disc_mp.parameters():
                        if p.grad is not None:
                            del p.grad  # free some memory
                    torch.cuda.empty_cache()
                    continue
                else:
                    raise e
            self.train_metrics.update("grad norm", self.get_grad_norm(self.gen))
            if batch_idx % self.log_step == 0:
                self.writer.set_step((epoch - 1) * self.len_epoch + batch_idx)

                self.writer.add_scalar(
                    "learning rate gen", self.gen_scheduler.get_last_lr()[0]
                )
                self.writer.add_scalar(
                    "learning rate disc", self.disc_scheduler.get_last_lr()[0]
                )
                self._log_spectrogram_audio(batch["melspec"], batch["melspec_gen"], batch["waveform"],
                                            batch["waveform_gen"])
                self._log_scalars(self.train_metrics)
            if batch_idx >= self.len_epoch:
                break
        log = self.train_metrics.result()

        if self.gen_scheduler is not None:
            self.gen_scheduler.step()
        if self.disc_scheduler is not None:
            self.disc_scheduler.step()

        if self.do_validation:
            val_log = self._valid_epoch(epoch)
            log.update(**{"val_" + k: v for k, v in val_log.items()})

        return log

    def process_batch(self, batch, is_train: bool, metrics: MetricTracker, gen_step=True):
        batch = self.move_batch_to_device(batch, self.device)
        batch["melspec"] = self.featurizer(batch["waveform"])
        batch["waveform_gen"] = self.gen(batch["melspec"])
        batch["melspec_gen"] = self.featurizer(batch["waveform_gen"])

        # MP losses

        out_real, out_gen, _, _ = self.disc_mp(batch["waveform"], batch["waveform_gen"].detach())
        loss_disc_mp, _, _ = self.criterion(out_real, out_gen, type="DiscriminatorLoss")

        _, out_gen, fmaps_real, fmaps_gen = self.disc_mp(batch["waveform"], batch["waveform_gen"])
        feat_loss = self.criterion(fmaps_real, fmaps_gen, type="FeatureLoss")
        gen_loss, _ = self.criterion(out_gen, type="GeneratorLoss")

        # MS losses

        out_real, out_gen, _, _ = self.disc_ms(batch["waveform"], batch["waveform_gen"].detach())
        loss_disc_ms, _, _ = self.criterion(out_real, out_gen, type="DiscriminatorLoss")

        _, out_gen, fmaps_real, fmaps_gen = self.disc_ms(batch["waveform"], batch["waveform_gen"])

        feat_loss += self.criterion(fmaps_real, fmaps_gen, type="FeatureLoss")
        gen_loss_, _ = self.criterion(out_gen, type="GeneratorLoss")
        gen_loss += gen_loss_

        mel_loss = 45 * self.criterion(batch["melspec"], batch["melspec_gen"])

        full_loss_gen = 2 * feat_loss + mel_loss + gen_loss

        disc_loss = loss_disc_mp + loss_disc_ms

        self.gen_opt.zero_grad()
        self.disc_opt.zero_grad()

        if is_train:
            if gen_step:
                full_loss_gen.backward()
                self._clip_grad_norm()
                self.gen_opt.step()
            else:
                disc_loss.backward()
                self._clip_grad_norm()
                self.disc_opt.step()

        metrics.update("mel_loss", mel_loss.item())
        metrics.update("feat_loss", feat_loss.item())
        metrics.update("gen_loss", gen_loss.item())
        metrics.update("disc_loss", disc_loss.item())

        return batch

    def _valid_epoch(self, epoch):
        """
        Validate after training an epoch

        :param epoch: Integer, current training epoch.
        :return: A log that contains information about validation
        """
        self.gen.eval()
        self.valid_metrics.reset()
        with torch.no_grad():
            for batch_idx, batch in tqdm(
                    enumerate(self.valid_data_loader),
                    desc="validation",
                    total=len(self.valid_data_loader),
            ):
                batch = self.process_batch(
                    batch,
                    is_train=False,
                    metrics=self.valid_metrics,
                )
            self.writer.set_step(epoch * self.len_epoch, "valid")
            self._log_scalars(self.valid_metrics)
            self._log_spectrogram_audio(batch["melspec"], batch["melspec_gen"], batch["waveform"],
                                        batch["waveform_gen"])

        # add histogram of model parameters to the tensorboard
        for name, p in self.gen.named_parameters():
            self.writer.add_histogram(name, p, bins="auto")
        for name, p in self.disc_mp.named_parameters():
            self.writer.add_histogram(name, p, bins="auto")
        for name, p in self.disc_ms.named_parameters():
            self.writer.add_histogram(name, p, bins="auto")
        return self.valid_metrics.result()

    def _progress(self, batch_idx):
        base = "[{}/{} ({:.0f}%)]"
        if hasattr(self.data_loader, "n_samples"):
            current = batch_idx * self.data_loader.batch_size
            total = self.data_loader.n_samples
        else:
            current = batch_idx
            total = self.len_epoch
        return base.format(current, total, 100.0 * current / total)

    def _log_spectrogram_audio(self, melspec, melspec_gen, waveform_true, waveform_gen):
        idx = random.choice(range(len(melspec)))
        melspec = melspec[idx]
        melspec_gen = melspec_gen[idx]
        melspec = melspec.cpu().detach()
        melspec_gen = melspec_gen.cpu().detach()
        image = PIL.Image.open(plot_spectrogram_to_buf(melspec))
        self.writer.add_image("melspec true", ToTensor()(image))
        image = PIL.Image.open(plot_spectrogram_to_buf(melspec_gen))
        self.writer.add_image("melspec pred", ToTensor()(image))

        self.writer.add_audio("audio true", waveform_true[idx].cpu(), 22050)
        self.writer.add_audio("audio pred", waveform_gen[idx].cpu(), 22050)

    @torch.no_grad()
    def get_grad_norm(self, model, norm_type=2):
        parameters = model.parameters()
        if isinstance(parameters, torch.Tensor):
            parameters = [parameters]
        parameters = [p for p in parameters if p.grad is not None]
        total_norm = torch.norm(
            torch.stack(
                [torch.norm(p.grad.detach(), norm_type).cpu() for p in parameters]
            ),
            norm_type,
        )
        return total_norm.item()

    def _log_scalars(self, metric_tracker: MetricTracker):
        if self.writer is None:
            return
        for metric_name in metric_tracker.keys():
            self.writer.add_scalar(f"{metric_name}", metric_tracker.avg(metric_name))
