import argparse
import collections
import os
import warnings
from pathlib import Path

import numpy as np
import torch
import torchaudio

import src.model as module_arch
from src.featurizer import MelSpectrogram, MelSpectrogramConfig
from src.utils import prepare_device
from src.utils.parse_config import ConfigParser

warnings.filterwarnings("ignore", category=UserWarning)

# fix random seeds for reproducibility
SEED = 123
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(SEED)


def main(config):
    gen = config.init_obj(config["generator"], module_arch)
    device, device_ids = prepare_device(config["n_gpu"])
    gen = gen.to(device)
    gen.eval()
    gen.load_state_dict(torch.load(config.resume)["state_dict_gen"])

    featurizer = MelSpectrogram(MelSpectrogramConfig())
    featurizer = featurizer.to(device)

    output_path = config["test"]["output_path"]
    Path(output_path).mkdir(parents=True, exist_ok=True)
    data_path = config["test"]["data_path"]

    for file in os.listdir(data_path):
        waveform, sr = torchaudio.load(os.path.join(data_path, file))

        melspec = featurizer(waveform.unsqueeze(0).to(device))
        output = gen(melspec)
        torchaudio.save(os.path.join(output_path, file), output.squeeze(0).cpu(), sr)


if __name__ == "__main__":
    args = argparse.ArgumentParser(description="PyTorch Template")
    args.add_argument(
        "-c",
        "--config",
        default=None,
        type=str,
        help="config file path (default: None)",
    )
    args.add_argument(
        "-r",
        "--resume",
        default=None,
        type=str,
        help="path to latest checkpoint (default: None)",
    )
    args.add_argument(
        "-d",
        "--device",
        default=None,
        type=str,
        help="indices of GPUs to enable (default: all)",
    )

    # custom cli options to modify configuration from default values given in json file.
    CustomArgs = collections.namedtuple("CustomArgs", "flags type target")
    options = [
        CustomArgs(["--lr", "--learning_rate"], type=float, target="optimizer;args;lr"),
        CustomArgs(
            ["--bs", "--batch_size"], type=int, target="data_loader;args;batch_size"
        ),
    ]
    config = ConfigParser.from_args(args, options)
    main(config)
