import argparse
import collections
import warnings

import numpy as np
import torch

import src.loss as module_loss
import src.model as module_arch
from src.datasets.utils import get_dataloaders
from src.featurizer import MelSpectrogram, MelSpectrogramConfig
from src.trainer import Trainer
from src.utils import prepare_device
from src.utils.parse_config import ConfigParser
import itertools

warnings.filterwarnings("ignore", category=UserWarning)

# fix random seeds for reproducibility
SEED = 123
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(SEED)


def main(config):
    logger = config.get_logger("train")

    # setup data_loader instances
    dataloaders = get_dataloaders(config)

    # build model architecture, then print to console
    gen = config.init_obj(config["generator"], module_arch)

    disc_ms = config.init_obj(config["ms_discriminator"], module_arch)
    disc_mp = config.init_obj(config["mp_discriminator"], module_arch)
    logger.info(gen)
    logger.info(disc_ms)
    logger.info(disc_mp)

    # prepare for (multi-device) GPU training
    device, device_ids = prepare_device(config["n_gpu"])
    gen = gen.to(device)
    disc_ms = disc_ms.to(device)
    disc_mp = disc_mp.to(device)

    if len(device_ids) > 1:
        gen = torch.nn.DataParallel(gen, device_ids=device_ids)
        disc_ms = torch.nn.DataParallel(disc_ms, device_ids=device_ids)
        disc_mp = torch.nn.DataParallel(disc_mp, device_ids=device_ids)

    # get function handles of loss and metrics
    loss_module = config.init_obj(config["loss"], module_loss).to(device)

    # build optimizer, learning rate scheduler. delete every lines containing lr_scheduler for disabling scheduler
    trainable_params = filter(lambda p: p.requires_grad, gen.parameters())
    opt_g = config.init_obj(config["optimizer"], torch.optim, trainable_params)

    opt_d = config.init_obj(config["optimizer"], torch.optim, itertools.chain(disc_ms.parameters(), disc_mp.parameters()))
    lr_scheduler_g = config.init_obj(config["lr_scheduler"], torch.optim.lr_scheduler, opt_g)
    lr_scheduler_d = config.init_obj(config["lr_scheduler"], torch.optim.lr_scheduler, opt_d)

    if not "val" in dataloaders:
        dataloaders["val"] = None

    # 1 batch
    if config["trainer"].get("one_batch", None) is not None:
        dataloaders["train"] = [next(iter(dataloaders["train"]))]

    featurizer = MelSpectrogram(MelSpectrogramConfig())
    featurizer = featurizer.to(device)

    trainer = Trainer(
        gen,
        disc_ms,
        disc_mp,
        loss_module,
        opt_g,
        opt_d,
        config=config,
        device=device,
        data_loader=dataloaders["train"],
        featurizer=featurizer,
        valid_data_loader=dataloaders["val"],
        lr_scheduler_g=lr_scheduler_g,
        lr_scheduler_d=lr_scheduler_d,
        len_epoch=config["trainer"].get("len_epoch", None)
    )

    trainer.train()


if __name__ == "__main__":
    args = argparse.ArgumentParser(description="PyTorch Template")
    args.add_argument(
        "-c",
        "--config",
        default=None,
        type=str,
        help="config file path (default: None)",
    )
    args.add_argument(
        "-r",
        "--resume",
        default=None,
        type=str,
        help="path to latest checkpoint (default: None)",
    )
    args.add_argument(
        "-d",
        "--device",
        default=None,
        type=str,
        help="indices of GPUs to enable (default: all)",
    )

    # custom cli options to modify configuration from default values given in json file.
    CustomArgs = collections.namedtuple("CustomArgs", "flags type target")
    options = [
        CustomArgs(["--lr", "--learning_rate"], type=float, target="optimizer;args;lr"),
        CustomArgs(
            ["--bs", "--batch_size"], type=int, target="data_loader;args;batch_size"
        ),
    ]
    config = ConfigParser.from_args(args, options)
    main(config)
